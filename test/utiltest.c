/* == TEST PROGRAM utiltest.c == */

/**
 * Tests the "util" module of the CryptoWrap library.
 * \author Andras Aszodi
 * \date 2013-06-09
 */

/* -- Own headers -- */

#include "cryptowrap/utils.h"
#include "cryptowrap/config.h"

/* -- Testing headers -- */

#include "check.h"

/* -- Standard headers -- */

#include <stdlib.h>
#include <string.h>

/* == TESTS == */

/* -- Key input tests -- */

START_TEST(readkey_priv)
{
    EVP_PKEY *dsaprivkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_dsa_priv.pem ), 
        1);
    ck_assert_ptr_ne(dsaprivkey, NULL);
    EVP_PKEY_free(dsaprivkey);
    
    EVP_PKEY *rsaprivkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_priv.pem ), 
        1);
    ck_assert_ptr_ne(rsaprivkey, NULL);
    EVP_PKEY_free(rsaprivkey);
}
END_TEST

START_TEST(readkey_pub)
{
    EVP_PKEY *dsapubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_dsa_pub.pem ), 
        0);
    ck_assert_ptr_ne(dsapubkey, NULL);
    EVP_PKEY_free(dsapubkey);
    
    EVP_PKEY *rsapubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_pub.pem ), 
        0);
    ck_assert_ptr_ne(rsapubkey, NULL);
    EVP_PKEY_free(rsapubkey);
}
END_TEST

/* -- Miscellaneous -- */

START_TEST(raw2hex)
{
    uint8_t RAW[4];
    char *hexstr = NULL;
    uint8_t *raw = NULL;
    unsigned int i, rawlen = 0;
    
    RAW[0] = 0xDE; RAW[1] = 0xAD; RAW[2] = 0xBE; RAW[3] = 0xEF;
    
    /* convert to hex string */
    hexstr = cryptowrap_raw2hex(RAW, 4);
    ck_assert_str_eq(hexstr, "deadbeef");
    
    /* convert back */
    raw = cryptowrap_hex2raw("deadbeef", &rawlen);
    ck_assert_int_eq(rawlen, 4);
    for (i = 0; i < 4; ++i) {
        ck_assert_int_eq(raw[i], RAW[i]);
    }
    free(hexstr);
    free(raw);
}
END_TEST

START_TEST(raw2base64)
{
    /* test based on `openssl enc -base64` commandline result */
    const unsigned int MESSAGELEN = 27;
    const uint8_t MESSAGE[] = "Liberte, egalite, absurdite";
    const char base64expected[] = "TGliZXJ0ZSwgZWdhbGl0ZSwgYWJzdXJkaXRl";
    char *base64result = NULL;
    uint8_t *decoded = NULL;
    unsigned int decodelen = 0;
    
    /* encode */
    base64result = cryptowrap_raw2base64(MESSAGE, MESSAGELEN);
    ck_assert_str_eq(base64result, base64expected);
    free(base64result);
    
    /* decode */
    decoded = cryptowrap_base642raw(base64expected, &decodelen);
    ck_assert_int_eq(decodelen, MESSAGELEN);
    ck_assert_str_eq(decoded, MESSAGE);
    free(decoded);
}
END_TEST

Suite *util_suite(void) {
    Suite *suite = suite_create("Utils");
    TCase *tc_readkey = tcase_create("Read keys"),
        *tc_misc = tcase_create("Misc");
    
    /* setup/teardown run only once per test case */
    tcase_add_unchecked_fixture (tc_readkey, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_readkey, readkey_priv);
    tcase_add_test(tc_readkey, readkey_pub);
    suite_add_tcase(suite, tc_readkey);
    
    /* the Misc test cases need no OpenSSL fixtures */
    tcase_add_test(tc_misc, raw2hex);
    tcase_add_test(tc_misc, raw2base64);
    suite_add_tcase(suite, tc_misc);
    
    return suite;
}

/* == MAIN == */

int main(int argc, char* argv[]) {
    int numfailed;
    Suite *suite = util_suite();
    SRunner *runner = srunner_create(suite);
    
    srunner_run_all (runner, CK_NORMAL);
    numfailed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (numfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
