#ifndef CRYPTOWRAP_PASSWORD_HEADER
#define CRYPTOWRAP_PASSWORD_HEADER

/* == HEADER cryptowrap/password.h == */

/** 
 * \file password.h
 * \brief Password generation utilities.
 * \author Andras Aszodi
 * \date 2013-06-14
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

#ifdef __cplusplus
extern "C" {
#endif

/* == Enums == */

/**
 * Enum constants for password character classes.
 */
typedef enum {
    PWD_LOWERONLY = 0,  /**< only lowercase letters: do not use! */
    PWD_WITHUPPER = 1,  /**< uppercase letters */
    PWD_WITHDIGIT = 2,  /**< digits */
    PWD_WITHSPEC = 4,  /**< special characters */
    PWD_WITHALL = 7     /**< `PWD_WITHUPPER | PWD_WITHDIGIT | PWD_WITHSPEC` */
} pwd_flags_t;

/* == Prototypes == */

/**
 * Generates a random password string.
 * \param password String to store the generated password. It is the caller's responsibility
 * to make sure that `password` is at least `passwordlen + 1` characters long. The contents
 * of `password` will be overwritten in each call.
 * \param passwordlen The length of the password, if 0, then a default length of 16 is used.
 * \param flags Define the character classes that must be present in the password by
 * bitwise OR-ing the following enum constants:

        PWD_WITHUPPER: uppercase letters A..Z
        PWD_WITHDIGIT: decimal digits 0..9
        PWD_WITHSPEC: special characters !#$%&*+/@?

 * There is always at least 1 lowercase character. To generate really unmemorizable passwords,
 * specify the `PWD_WITHALL` flag which is `PWD_WITHUPPER | PWD_WITHDIGIT | PWD_WITHSPEC`.
 * \return The 1 upon success, 0 on error.
 */
int cryptowrap_genpassword(
        char *password,
        unsigned int passwordlen,
        int flags
);

/**
 * Generates a password hash using PBKDF2.
 * \param password The password string, \0-terminated
 * \param salt An array of raw bytes
 * \param saltlen The length of `salt`
 * \param itercount Hashing iteration count, if 0 then a default value (1000) is used
 * \param hashlen The desired length of the password hash
 * \param hash An array of raw bytes, must be at least `hashlen` long. Upon successful
 * completion it will hold the hashed password.
 * \return 1 on success, 0 on error (OpenSSL retval convention).
 */
int cryptowrap_passwordhash(
        const char* password,
        const uint8_t *salt,
        unsigned int saltlen,
        unsigned int itercount,
        unsigned int hashlen,
        uint8_t *hash
);

#endif  /* CRYPTOWRAP_PASSWORD_HEADER */
