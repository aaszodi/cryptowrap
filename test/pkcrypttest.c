/* == TEST PROGRAM pkcrypttest.c == */

/**
 * Tests the "pkcrypt" module of the CryptoWrap library.
 * \author Andras Aszodi
 * \date 2013-06-12
 */

/* -- Own headers -- */

#include "cryptowrap/pkcrypt.h"
#include "cryptowrap/utils.h"
#include "cryptowrap/config.h"

/* -- Testing headers -- */

#include "check.h"

/* -- Standard headers -- */

#include <stdlib.h>

/* == TESTS == */

static const char CIPHER[] = "aes-256-cbc";

/* -- Encryption and decryption -- */

/* encrypt/decrypt string w/o \0, test envelope details */
START_TEST(pkcrypt_str1)
{
    const uint8_t MESSAGE[63] = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ\0";  
    const unsigned int MAXMESSAGELEN = 62; /* terminating \0 won't be encrypted */
    
    EVP_PKEY *privkey, *pubkey;
    cryptowrap_envelope_t env; /* contains rubbish, will be init'ed by cryptowrap_pkencrypt */
    uint8_t *plain = NULL;
    unsigned int plainlen = 0;
    int retval;
    
    /* encryption */
    pubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_pub.pem ), 
        0);
    ck_assert_ptr_ne(pubkey, NULL);
    retval = cryptowrap_pkencrypt(
        MESSAGE, MAXMESSAGELEN,
        CIPHER, pubkey,
        &env
    );
    ck_assert_int_eq(retval, 1);
    ck_assert_ptr_ne(env.crypt, NULL);
    ck_assert_int_ne(env.cryptlen, 0);
    ck_assert_str_eq(env.ciphername, CIPHER);
    
    /* decrypt and compare plaintext */
    privkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_priv.pem ), 
        1);
    ck_assert_ptr_ne(privkey, NULL);
    
    plain = cryptowrap_pkdecrypt(
        &env, privkey, &plainlen
    );
    ck_assert_ptr_ne(plain, NULL);
    ck_assert_int_eq(plainlen, MAXMESSAGELEN);
    
    /* in order to do string comparison for testing,
     * we artificially terminate the _plain_ buffer
     */
    plain[plainlen] = '\0';
    ck_assert_str_eq((const char*) plain, (const char*) MESSAGE);
    
    /* test the envelope cleanup routine */
    cryptowrap_clear_envelope(&env);
    ck_assert_ptr_eq(env.crypt, NULL);
    ck_assert_int_eq(env.cryptlen, 0);
    ck_assert_ptr_eq(env.ciphername, NULL);
    ck_assert_ptr_eq(env.eckey, NULL);
    ck_assert_int_eq(env.eckeylen, 0);
    
    free(plain); 
    EVP_PKEY_free(privkey);
    EVP_PKEY_free(pubkey);
}
END_TEST

/* encrypt/decrypt strings with \0, vary by length */
START_TEST(pkcrypt_strings)
{
    const uint8_t AZ[] = "abcdefghijklmnopqrstuvwxyz";   /* 26 chars */
    const unsigned int AZLEN = 26, AZREP = 7;
    uint8_t *message = NULL; /* put AZ AZREP times into this */
    
    EVP_PKEY *privkey, *pubkey;
    cryptowrap_envelope_t env; /* contains rubbish, will be init'ed by cryptowrap_pkencrypt */
    uint8_t *plain = NULL;
    unsigned int rep, clen, plainlen = 0;
    int retval;
    
    /* Get the keys. Not tested */
    pubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_pub.pem ), 
        0);
    privkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_priv.pem ), 
        1);

    /* create a long message */
    message = (uint8_t *) calloc(AZLEN * AZREP + 1, sizeof(uint8_t));
    for (rep = 0; rep < AZREP; ++rep) {
        memcpy(message + rep * AZLEN, AZ, AZLEN);
    }
    for (clen = AZLEN * AZREP; clen > 0; --clen) {
        message[clen - 1] = '\0';   /* terminate to make shorter */
        retval = cryptowrap_pkencrypt(
            message, clen,
            CIPHER, pubkey,
            &env
        );

        /* decrypt and compare plaintext */    
        plain = cryptowrap_pkdecrypt(
            &env, privkey, &plainlen
        );
        ck_assert_ptr_ne(plain, NULL);
        ck_assert_int_eq(plainlen, clen);
        ck_assert_str_eq((const char*) plain, (const char*) message);
        
        free(plain);
        cryptowrap_clear_envelope(&env);
    }
    
    free(message);
    EVP_PKEY_free(privkey);
    EVP_PKEY_free(pubkey);
}
END_TEST

Suite *util_suite(void) {
    Suite *suite = suite_create("PubCrypt");
    TCase *tc_pkcrypt = tcase_create("PubCrypt");
    
    /* OpenSSL init/cleanup runs only for the whole test case */
    tcase_add_unchecked_fixture (tc_pkcrypt, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_pkcrypt, pkcrypt_str1);
    tcase_add_test(tc_pkcrypt, pkcrypt_strings);
    suite_add_tcase(suite, tc_pkcrypt);
    
    return suite;
}

/* == MAIN == */

int main(int argc, char* argv[]) {
    int numfailed;
    Suite *suite = util_suite();
    SRunner *runner = srunner_create(suite);
    
    srunner_run_all (runner, CK_NORMAL);
    numfailed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (numfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
