#ifndef CRYPTOWRAP_SYMKEY_HEADER
#define CRYPTOWRAP_SYMKEY_HEADER

/* == HEADER cryptowrap/symkey.h == */

/** 
 * \file symkey.h
 * \brief Symmetric key handling.
 * \author Andras Aszodi
 * \date 2013-06-19
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"

/* -- OpenSSL headers -- */

#ifdef __cplusplus
extern "C" {
#endif

/* == Structs == */

/**
 * The cryptowrap_symkey_t struct encapsulates 
 * a symmetric encryption key, its init vector and a salt.
 * All are stored in the same dynamically allocated byte array,
 * the key in `key[0]` to `key[keylen-1]`, the IV in `key[keylen]` to `key[keylen+ivlen-1]`,
 * and the salt in key[keylen+ivlen] to key[keylen+ivlen+saltlen-1].
 * The `iv` member always points to `key[keylen]` or to `NULL` if there is no IV (`ivlen == 0`).
 * The `salt` member always points to `key[keylen+ivlen]` or to `NULL`
 * if there is no salt (`saltlen == 0`).
 * Symkey structs must be initialized before use and destroyed after use. Example:
 
        cryptowrap_symkey_t symkey;
        ...
        cryptowrap_symkey_init(&symkey, keylen, ivlen, saltlen);
        ... work with symkey...
        cryptowrap_symkey_destroy(&symkey);
 
 */
typedef struct {
    uint8_t *key;     /**< the [key | iv | salt] combined raw byte array */
    unsigned int keylen;  /**< length of `key`, `keylen > 0` */
    uint8_t *iv;  /**< init vector, `iv == key + keylen` */
    unsigned int ivlen;     /**< length of `iv` */
    uint8_t *salt;  /**< random salt, `salt = key + keylen + ivlen` */
    unsigned int saltlen;   /**< length of `salt` */
} cryptowrap_symkey_t;

/* == Prototypes == */

/**
 * Initializes a `cryptowrap_symkey_t` structure for use.
 * \param symkey Pointer to the key structure to be initialized. Upon return
 * structure will have the proper internal setup. All vectors are zeroed.
 * \param keylen The key vector length, must be > 0
 * \param ivlen The init vector (IV) length, may be 0
 * \param saltlen The salt length, may be 0
 * \return The sum of `keylen`,`ivlen`,`saltlen` upon success, 0 on failure.
 */
unsigned int cryptowrap_init_symkey(
        cryptowrap_symkey_t *symkey,
        unsigned int keylen,
        unsigned int ivlen,
        unsigned int saltlen
);

/**
 * Clears up the storage associated with a `cryptowrap_symkey_t` structure.
 * You may invoke `cryptowrap_init_symkey(&symkey)` again to re-use the structure itself.
 * \param symkey Pointer to the structure to be cleared.
 */
void cryptowrap_destroy_symkey(cryptowrap_symkey_t *symkey);

#endif  /* CRYPTOWRAP_SYMKEY_HEADER */
