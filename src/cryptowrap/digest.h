#ifndef CRYPTOWRAP_DIGEST_HEADER
#define CRYPTOWRAP_DIGEST_HEADER

/* == HEADER cryptowrap/digest.h == */

/** 
 * \file digest.h
 * \brief In-memory message digest calculations.
 * \author Andras Aszodi
 * \date 2013-06-10
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

/* == Prototypes == */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Calculates a message digest for in-memory messages.
 * \param message The message to be digested. It may be binary.
 * \param message_len The length of _message_. You have to be honest here.
 * \param digestname The name of the digest to be used, e.g. "sha512".
 * \return The raw binary digest allocated inside. The length of the digest is in _digest_len_.
 * Do not forget to deallocate the result. NULL is returned upon errors.
 */
uint8_t* cryptowrap_digest(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        unsigned int *digest_len
);

/**
 * Digitally signs an in-memory message.
 * \param message The message to be signed. It may be binary.
 * \param message_len The length of _message_. You have to be honest here.
 * \param digestname The name of the digest to be used for the signature, e.g. "sha512".
 * \param privkey The private key used to encrypt the digest.
 * \return The raw binary signature allocated inside. The length of the signature is in _signature_len_.
 * Do not forget to deallocate the result. NULL is returned upon errors.
 */
uint8_t* cryptowrap_sign(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        const EVP_PKEY *privkey,
        unsigned int *signature_len
);

/**
 * Verifies a digitally signed message in memory.
 * \param message The signed message to be verified. It may be binary.
 * \param message_len The length of _message_. You have to be honest here.
 * \param digestname The name of the digest that was used for signing, e.g. "sha512".
 * \param signature The raw binary signature to be verified
 * \param signature_len The length of the signature
 * \param pubkey The public key used to decrypt the digest.
 * \return 1 if verification succeeded, 0 if failed, -1 on errors.
 */
int cryptowrap_verify(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        const uint8_t *signature,
        unsigned int signature_len,
        const EVP_PKEY *pubkey
);

#ifdef __cplusplus
}
#endif

#endif  /* CRYPTOWRAP_DIGEST_HEADER */
