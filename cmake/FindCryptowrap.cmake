# Find the Cryptowrap OpenSSL wrapper library
# This script also finds the OpenSSL library itself,
# to make sure there's something Cryptowrap can wrap :-)
#
# 2013-08-04 Andras Aszodi

# This script understands the following variables:
# OPENSSL_ROOT_DIR to help CMake find OpenSSL at a non-standard location
# OPENSSL_USE_DYNAMIC_LIBS if you wish to link OpenSSL dynamically
# LIBCRYPTOWRAP_ROOT to help CMake find the Cryptowrap library at a non-standard location
# LIBCRYPTOWRAP_USE_DYNAMIC_LIBS if you wish to link the library dynamically

# The following variables will be set by this script upon return:
# LIBCRYPTOWRAP_FOUND: true if both OpenSSL and Cryptowrap are found
# LIBCRYPTOWRAP_INCLUDE_DIRS: header location
# LIBCRYPTOWRAP_LIBRARY_DIRS: library location
# LIBCRYPTOWRAP_LIBRARY: the library to be linked

set(LIBCRYPTOWRAP_FOUND FALSE)

# -- Find OpenSSL first --

set(OPENSSL_ROOT_DIR "/usr/local/openssl" CACHE PATH "Top-level OpenSSL directory")
option(OPENSSL_USE_DYNAMIC_LIBS "Shall the OpenSSL libraries be linked dynamically?" ${USE_DYNAMIC_LIBS})
find_package( OpenSSL REQUIRED )    # finder script supplied by CMake
if(OPENSSL_FOUND)
    # Apple deprecates OpenSSL in OS X "Lion" and above
    # here is an ad-hoc fix by pretending we're still on Snow Leopard
    if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin" AND ${CMAKE_SYSTEM_VERSION} VERSION_GREATER "10")
        add_definitions(-DMAC_OS_X_VERSION_MIN_REQUIRED=MAC_OS_X_VERSION_10_6)
    endif()
else(OPENSSL_FOUND)
    message(FATAL_ERROR "OpenSSL was not found: use -DOPENSSL_ROOT_DIR=/path/to/OpenSSL to help CMake")
endif(OPENSSL_FOUND)

# -- Finding LibCryptowrap itself --

# top-level directory location
# identify the library by its config header
set(LIBCRYPTOWRAP_HEADER "/include/cryptowrap/config.h")
if (LIBCRYPTOWRAP_ROOT)
    # force to look in user-defined location
    find_path(LIBCRYPTOWRAP_TOPLEVEL ${LIBCRYPTOWRAP_HEADER} PATHS ${LIBCRYPTOWRAP_ROOT} NO_DEFAULT_PATH)
endif()
if (NOT LIBCRYPTOWRAP_TOPLEVEL)
    # still not found, look in standard locations
    find_path(LIBCRYPTOWRAP_TOPLEVEL ${LIBCRYPTOWRAP_HEADER})
endif()
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LIBCRYPTOWRAP_TOPLEVEL DEFAULT_MSG "LIBCRYPTOWRAP_TOPLEVEL")

if (LIBCRYPTOWRAP_TOPLEVEL)
    set(LIBCRYPTOWRAP_INCLUDE_DIRS ${LIBCRYPTOWRAP_TOPLEVEL}/include)
    set(LIBCRYPTOWRAP_LIBRARY_DIRS ${LIBCRYPTOWRAP_TOPLEVEL}/lib)
    
    # library location
    if (LIBCRYPTOWRAP_USE_DYNAMIC_LIBS)
        set(LIBNAMES "cryptowrap" "libcryptowrap")
    else()
        set(LIBNAMES "libcryptowrap.a")
    endif()
    find_library(LIBCRYPTOWRAP_LIBRARY
        NAMES ${LIBNAMES}
        PATHS ${LIBCRYPTOWRAP_LIBRARY_DIRS}
    )
    if (LIBCRYPTOWRAP_LIBRARY)
        set(LIBCRYPTOWRAP_FOUND TRUE)
        list(APPEND LIBCRYPTOWRAP_INCLUDE_DIRS ${OPENSSL_INCLUDE_DIR})
        list(APPEND LIBCRYPTOWRAP_LIBRARY_DIRS ${OPENSSL_LIBRARY_DIRS})
    endif()
endif()
mark_as_advanced(LIBCRYPTOWRAP_INCLUDE_DIRS LIBCRYPTOWRAP_LIBRARY )
