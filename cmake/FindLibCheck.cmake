# Find the Check C unit testing library
# http://check.sourceforge.net
#
# 2013-06-07 Andras Aszodi

# The following variables will be set:
# LIBCHECK_FOUND: true if found
# LIBCHECK_INCLUDE_DIRS: header location
# LIBCHECK_LIBRARY_DIRS: library location
# LIBCHECK_LIBRARY: the library to be linked
# 
# Define -DLIBCHECK_ROOT to help CMake find the Check library at a non-standard location

set(LIBCHECK_FOUND FALSE)

# top-level directory location
if (LIBCHECK_ROOT)
    # force to look in user-defined location
    find_path(LIBCHECK_TOPLEVEL /include/check.h PATHS ${LIBCHECK_ROOT} NO_DEFAULT_PATH)
endif()
if (NOT LIBCHECK_TOPLEVEL)
    # still not found, look in standard locations
    find_path(LIBCHECK_TOPLEVEL /include/check.h)
endif()
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LIBCHECK_TOPLEVEL DEFAULT_MSG "LIBCHECK_TOPLEVEL")

if (LIBCHECK_TOPLEVEL)
    set(LIBCHECK_INCLUDE_DIRS ${LIBCHECK_TOPLEVEL}/include)
    set(LIBCHECK_LIBRARY_DIRS ${LIBCHECK_TOPLEVEL}/lib)
    
    # library location
    if (LIBCHECK_USE_DYNAMIC_LIBS)
        set(LIBNAMES "check" "libcheck")
    else()
        set(LIBNAMES "libcheck.a")
    endif()
    find_library(LIBCHECK_LIBRARY
        NAMES ${LIBNAMES}
        PATHS ${LIBCHECK_LIBRARY_DIRS}
    )
    if (LIBCHECK_LIBRARY)
        set(LIBCHECK_FOUND TRUE)
    endif()
endif()
mark_as_advanced(LIBCHECK_INCLUDE_DIRS LIBCHECK_LIBRARY )
