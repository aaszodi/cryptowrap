CRYPTOWRAP LIBRARY INSTALLATION INSTRUCTIONS
============================================

Prerequisites
-------------

* An ANSI C compiler
* OpenSSL 
* CMake version 2.8 or above
* [optional:] The Libcheck C unit test library for the debug version

Building and installing the release version
-------------------------------------------

Starting from the top-level directory issue the following commands:

    mkdir -p build/release
    cd build/release
    cmake -DCMAKE_BUILD_TYPE=Release \
        -DOPENSSL_ROOT_DIR=/path/to/openssl \
        -DCMAKE_INSTALL_PREFIX=/path/to/cryptowrap ../..
    make libs
    make apps
    make doc
    make install
    
This will build the static and shared libraries and the helper applications. The apps will
be linked statically by default. You may specify `-DUSE_DYNAMIC_LIBS=ON` to use
dynamic linking throughout.

You need to define the location of OpenSSL only if it is installed in some non-standard
location.

Building and installing the development version
-----------------------------------------------

The development version is built like this:

    mkdir -p build/debug
    cd build/debug
    cmake -DCMAKE_BUILD_TYPE=Debug \
        -DOPENSSL_ROOT_DIR=/path/to/openssl \
        -DLIBCHECK_ROOT=/path/to/libcheck ../..
    make cryptowrap_static
    make unit_tests
    
where the appropriate path to the Check library needs to be specified if it is 
in a non-standard location. We do not specify an installation prefix here
because the development version cannot be installed.

The unit tests require the statically built library only. To build the shared version,
issue the command `make cryptowrap_shared`.

To run the unit tests, issue the command `make test`, this will invoke CTest. 
For some reason this does not seem to work on Solaris, there you would need to
run the tests manually: they are all in the `build/debug/test` subdirectory.




