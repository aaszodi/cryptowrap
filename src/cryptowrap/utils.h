#ifndef CRYPTOWRAP_UTILS_HEADER
#define CRYPTOWRAP_UTILS_HEADER

/* == HEADER cryptowrap/utils.h == */

/** 
 * \file utils.h
 * \brief Utility functions for wrapping OpenSSL.
 * \author Andras Aszodi
 * \date 2013-06-09
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"

/* -- OpenSSL headers -- */

#include "openssl/engine.h"
#include "openssl/pem.h"

/* -- Standard headers -- */

/* == Prototypes == */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initializes OpenSSL.
 * This function must be invoked first by a program using Cryptowrap.
 */
void cryptowrap_init(void);

/**
 * Checks OpenSSL errors and prints human-readable error messages to stderr.
 * \param funcname The name of the function whose return value is being checked
 * \param retval The value returned by the function
 * \param successval The return value expected upon success
 * \return An error number as returned by `ERR_get_error()` or 0 upon success.
 */
int cryptowrap_errorcheck(
        const char* funcname,
        int retval,
        int successval
);

/**
 * Reads a public or private key from a PEM file.
 * This routine is for batch operations therefore only password-less private keys can be read.
 * \param filename The name of the key file
 * \param keytype 0 = public key, any other int = private key
 * \return an `EVP_PKEY` structure pointer on success or `NULL` on failure.
 */
EVP_PKEY *cryptowrap_readkey(
        const char *filename,
        int keytype
);

/**
 * Allocates and returns a string containing the hex representation of raw bytes.
 * \param raw array containing the raw bytes
 * \param rawlen the length of the _raw_ array
 * \return a 2*rawlen+1 -long char array containing the hex representation of _raw_.
 * Do not forget to deallocate it after use.
 */
char* cryptowrap_raw2hex(
        const uint8_t *raw,
        unsigned int rawlen
);

/**
 * Allocates and returns a raw byte array containing the decoded value of the
 * input hex string. This is the inverse operation of cryptowrap_raw2hex().
 * \param hexstr The hexadecimal ASCII string.
 * \param rawlen Pointer to the length of the returned raw byte array (`strlen(hexstr)/2`).
 * \return The decoded raw byte array, allocated within. Do not forget to deallocate
 * after use.
 */
uint8_t* cryptowrap_hex2raw(
        const char *hexstr,
        unsigned int *rawlen
);

/**
 * Allocates and returns a string containing the Base64 representation of raw bytes.
 * The string has no line breaks in it.
 * \param rawlen the length of the _raw_ array
 * \return a \0-terminated contiguous string with the Base64 encoding or _raw_.
 * Do not forget to deallocate it after use.
 */
char* cryptowrap_raw2base64(
        const uint8_t *raw,
        unsigned int rawlen
);

/**
 * Allocates and returns a raw byte array containing the decoded value of the
 * input base64-encoded string. This is the inverse operation of cryptowrap_raw2base64().
 * \param b64str Base64-encoded string (\0-terminated, without newlines)
 * \param rawlen Pointer to the length of the raw byte array returned by the function
 * \return The decoded raw byte array, allocated within. Do not forget to
 * deallocate after use.
 */
uint8_t *cryptowrap_base642raw(
    const char* b64str,
    unsigned int* rawlen
);

/**
 * Removes all OpenSSL-related data.
 * This function must be invoked last by a program using Cryptowrap.
 */
void cryptowrap_cleanup(void);

#ifdef __cplusplus
}
#endif

#endif  /* CRYPTOWRAP_UTILS_HEADER */
