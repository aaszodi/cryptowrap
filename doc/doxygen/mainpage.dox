// Standalone Doxygen documentation, following the advice from
// http://entrenchant.blogspot.com/2009/09/doxygen-gotchas.html

/**
\mainpage Simple OpenSSL wrapper library

\section intro_sec Overview and Rationale

This library is intended to help using the [OpenSSL](http://openssl.org) API for common tasks
such as creating a digest, signing a message, or encrypting / decrypting data.
It operates on in-memory raw data and leaves the responsibility of reading/writing messages, digests
and signatures to the calling program. A notable exception is the reading
of public and private keys for which input routines are provided.

I wrote this library to learn how to use OpenSSL in practical situations.
OpenSSL is frighteningly under-documented, and a lot of trial and error is involved
in figuring out how to perform everyday crypto tasks. I tried to hide these
details in CryptoWrap. The library does not otherwise claim usefulness of any kind.

\section install_sec Installation

\subsection prereqs_subsec Prerequisites

You will need an ANSI C compiler and the [CMake](http://cmake.org/) building system.
At least CMake Version 2.8 is required.

The only other mandatory dependency is that the OpenSSL libraries and headers
be installed on the machine (obviously). If they are installed in a non-standard location,
then CMake will not find them. In that case, supply `-DOPENSSL_ROOT_DIR=/path/to/openssl`
when invoking CMake.

For developers only: the unit tests rely on the [Check library](http://check.sourceforge.net/).

\subsection build_subsec Building CryptoWrap

Building a productive version of `cryptowrap` follows the usual CMake workflow.
My build convention is to have separate `debug` and `release` subdirectories under
a `build` directory. This convention is used in the instructions below but of course
you can name your build directory as you like.

Starting from the top-level directory issue the following commands:

    mkdir -p build/release
    cd build/release
    cmake -DCMAKE_BUILD_TYPE=Release \
        -DOPENSSL_ROOT_DIR=/path/to/openssl \
        -DCMAKE_INSTALL_PREFIX=/path/to/cryptowrap ../..
    make libs
    make apps
    make doc
    make install
    
This will build the static and shared libraries and the helper applications. The apps will
be linked statically by default. You may specify `-DUSE_DYNAMIC_LIBS=ON` to use
dynamic linking throughout.

The development version is built like this:

    mkdir -p build/debug
    cd build/debug
    cmake -DCMAKE_BUILD_TYPE=Debug \
        -DOPENSSL_ROOT_DIR=/path/to/openssl \
        -DLIBCHECK_ROOT=/path/to/libcheck ../..
    make cryptowrap_static
    make unit_tests
    
where the appropriate path to the Check library needs to be specified if it is 
in a non-standard location. We do not specify an installation prefix here, the
purpose of the debug build is to develop the code.

CryptoWrap comes with quite some unit tests. They need only the static version
of the library. To run the unit tests via CTest, invoke the `make test` command.
Note that for some reason this command does not work on Solaris, and you need 
elevated privileges to run the test programs manually because the `timer_create()`
function used by Check requires that.

\section usage_sec Using CryptoWrap

\subsection libusage_subsec Using the library routines

To build applications with CMake that use Cryptowrap, take a look at the file
`cmake/FindCryptowrap.cmake`. This can be used in a top-level `CMakeLists.txt`
in the usual manner to locate both OpenSSL and Cryptowrap.

Example programs will be provided.

\subsection appusage_subsec Using the helper applications

\subsubsection cryptowrap_config_subsubsec The cryptowrap_config program

This program simply prints configuration details. Might be useful when writing
a CMake FindXXXX utility for CryptoWrap.

\subsubsection pwgen_subsubsec The pwgen password generator program

This program generates random passwords using OpenSSL's "cryptography-strength"
random number generator. You can set the minimum length,
the number of passwords it generates in one invocation, and the character classes
to be included in the output. Here is how you can invoke it, based on its
built-in help:

    Usage: pwgen -udsa [-L pwdlen] [-n pwdcount]
    Character set selection options:
        -u: Require at least one uppercase character
        -d: Require at least one digit character
        -s: Require at least one special character
        -a: Equivalent to -uds
    Password length and multiple password options:
        -L pwdlen: password length, default 16 characters
        -n pwdcount: generate this many passwords, default 1
        -v: Print version information and exit
        -h: Print this help and exit

To generate 5 passwords, each 14 characters long, containing lower- and uppercase
characters, digits and special characters, invoke the program like this:

    pwgen -a -L 14 -n 5

The output will look like this (your results will be different strings though):

    dO6DB938Jq?p$S
    &TBD+3K9+px4Pk
    Sy2qfaa&85v0v1
    rmnp4CS#TTQ8&f
    i&Q1O*C3F+/ARr

By default 16-character-long passwords are generated which should be OK for most purposes.

\subsection scriptusage_subsec Using the helper scripts

\subsubsection keygen_subsubsec The keygen.sh public/private keypair generator

This Bash script just invokes the appropriate OpenSSL command-line incantations to generate
public/private keypairs. It does _not_ rely on the CryptoWrap library. I included it in
the distribution because I thought it would be useful. The test keys supplied with
CryptoWrap were made with this little tool. Here is how to invoke it, based on its
built-in help:

    keygen.sh <type> <prefix> [<numbits>]
      <type> = dsa | rsa
      <prefix>: key file prefix, the output files are <prefix>_<type>_{priv|pub}.pem
      <numbits>: number of bits, default 1024

Note that the script won't work if it cannot find the `openssl` command in your `$PATH`.

*/

