#!/bin/bash

# Generates DSA or RSA keypairs using OpenSSL.
# The private keys are _NOT_ password-protected.
# Andras Aszodi
# 2013-06-09

NUMBITS=1024

print_help() {
    PROGNAME=`basename $0`
    echo "Generate a DSA or RSA keypairs in PEM format. Usage:"
    echo "$PROGNAME <type> <prefix> [<numbits>]"
    echo "  <type> = dsa | rsa"
    echo "  <prefix>: key file prefix, the output files are <prefix>_<type>_{priv|pub}.pem"
    echo "  <numbits>: number of bits, default ${NUMBITS}"
}

which -s openssl
if [ $? != 0 ]; then
    echo "openssl is not in PATH, exiting"
    exit 2
fi

if [ $# -lt 2 ]; then
    print_help
    exit 1
fi
if [ $# -ge 3 ]; then
    NUMBITS=$3
fi

TYPE=$1
PREFIX=$2
PRIVKEYFILE=${PREFIX}_${TYPE}_priv.pem
PUBKEYFILE=${PREFIX}_${TYPE}_pub.pem
case $TYPE in
    "dsa")
        PARAMFILE=`mktemp /tmp/dsaparamXXXXXX`
        openssl dsaparam -out $PARAMFILE $NUMBITS
        openssl gendsa -out $PRIVKEYFILE $PARAMFILE
        rm $PARAMFILE
        openssl dsa -in $PRIVKEYFILE -pubout -out $PUBKEYFILE
        ;;
    "rsa")
        openssl genrsa -out $PRIVKEYFILE $NUMBITS
        openssl rsa -in $PRIVKEYFILE -pubout -out $PUBKEYFILE
        ;;
    *)
        echo "Key type must be 'dsa' or 'rsa'"
        ;;
esac


