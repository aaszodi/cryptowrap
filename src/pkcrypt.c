/* == MODULE pkcrypt.c == */

/** 
 * Public/private key-based encryption.
 * \author Andras Aszodi
 * \date 2013-06-12
 */

/* -- Own header -- */

#include "cryptowrap/pkcrypt.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

/* -- Standard headers -- */

#include <stdio.h>
#include <string.h>

/* == Implementation == */

int cryptowrap_pkencrypt(
        const uint8_t *plain,
        unsigned int plainlen,
        const char* ciphername,
        const EVP_PKEY *pubkey,
        cryptowrap_envelope_t *envp
) {
    EVP_CIPHER_CTX ctx;
    const EVP_CIPHER *cipher = NULL;
    unsigned int clen = 0;  
    int retval = 1, error = 0;
    
    /* init the output envelope */
    envp->crypt = NULL;
    envp->cryptlen = 0;
    envp->ciphername = NULL;
    envp->eckey = NULL;
    envp->eckeylen = 0;
    
    if (EVP_PKEY_type(((EVP_PKEY*)pubkey)->type) != EVP_PKEY_RSA) {
        fputs("! cryptowrap_pubencrypt(): Key type must be RSA\n", stderr);
        return 0;
    }
    
    cipher = EVP_get_cipherbyname(ciphername);
    if (cipher == NULL) {
        fprintf(stderr, "! cryptowrap_pubencrypt(): Cipher \"%s\" not found.", ciphername);
        return 0;
    }
    envp->ciphername = (char *) calloc(strlen(ciphername) + 1, sizeof(char));
    strcpy(envp->ciphername, ciphername);
    
    /* init */
    EVP_CIPHER_CTX_init(&ctx);
    envp->eckey = (uint8_t *) malloc(EVP_PKEY_size(pubkey));
    retval = EVP_SealInit(&ctx, cipher, 
        &(envp->eckey), &(envp->eckeylen), 
        &(envp->iv), &pubkey, 1);
    error = cryptowrap_errorcheck("EVP_SealInit()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* encrypt. This could be repeated for several rounds */
    envp->crypt = (uint8_t *) malloc(plainlen + EVP_MAX_IV_LENGTH);
    retval = EVP_SealUpdate(&ctx, 
        envp->crypt + envp->cryptlen,   /* part of buffer that's still empty */
        &clen,      /* this much is written in this round */
        plain, plainlen
    );
    envp->cryptlen += clen; /* keep track of bytes written in this round */
    error = cryptowrap_errorcheck("EVP_SealUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finish. This also writes to the end of the buffer */
    retval = EVP_SealFinal(&ctx, 
        envp->crypt + envp->cryptlen, 
        &clen
    );
    envp->cryptlen += clen; /* keep track of bytes written in the finalization round */
    error = cryptowrap_errorcheck("EVP_SealFinal()", retval, 1);
    
    CLEANUP_RETURN:
    if (error) {
        cryptowrap_clear_envelope(envp);
    }
    EVP_CIPHER_CTX_cleanup(&ctx);
    return retval;
}

uint8_t* cryptowrap_pkdecrypt(
        const cryptowrap_envelope_t *envp,
        const EVP_PKEY *privkey,
        unsigned int *plainlen
) {
    EVP_CIPHER_CTX ctx;
    const EVP_CIPHER *cipher = NULL;
    uint8_t *plain = NULL;  /* decrypted raw bytes */
    unsigned int plen = 0;
    int retval, error = 0;
    
    *plainlen = 0;
    if (EVP_PKEY_type(((EVP_PKEY*)privkey)->type) != EVP_PKEY_RSA) {
        fputs("! cryptowrap_pubdecrypt(): Key type must be RSA\n", stderr);
        return NULL;
    }
    
    cipher = EVP_get_cipherbyname(envp->ciphername);
    if (cipher == NULL) {
        fprintf(stderr, "! cryptowrap_pubdecrypt(): Cipher \"%s\" not found.", envp->ciphername);
        return NULL;
    }
    
    /* init */
    EVP_CIPHER_CTX_init(&ctx);
    retval = EVP_OpenInit(&ctx, cipher, envp->eckey, envp->eckeylen, envp->iv, privkey);
    error = cryptowrap_errorcheck("EVP_OpenInit()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* decrypt. This could be repeated for several rounds */
    plain = (uint8_t *) malloc(envp->cryptlen + EVP_MAX_IV_LENGTH);
    retval = EVP_OpenUpdate(&ctx, 
        plain + *plainlen,  /* offset to output buffer empty part */
        &plen,      /* this much is written in this round */
        envp->crypt, envp->cryptlen
    );
    *plainlen += plen;  /* keep track of output buffer length */
    error = cryptowrap_errorcheck("EVP_OpenUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finish */
    retval = EVP_OpenFinal(&ctx, 
        plain + *plainlen,
        &plen
    );
    *plainlen += plen;  /* final round */
    error = cryptowrap_errorcheck("EVP_OpenFinal()", retval, 1);
    
    CLEANUP_RETURN:
    if (error) {
        free(plain);
        plain = NULL;
        *plainlen = 0;
    }
    EVP_CIPHER_CTX_cleanup(&ctx);
    return plain;
}

void cryptowrap_clear_envelope(cryptowrap_envelope_t *envp) {
    free(envp->crypt); envp->crypt = NULL;
    envp->cryptlen = 0;
    free(envp->ciphername); envp->ciphername = NULL;
    free(envp->eckey); envp->eckey = NULL;
    envp->eckeylen = 0;
}
