# == test/CMakeLists.txt ==

# Unit tests. These are built only if CMAKE_BUILD_TYPE=Debug and the 'check' library was found.
# The test programs link the static version of the CryptoWrap library.
# 2013-06-09 Andras Aszodi

# Select the appropriate cryptowrap library variant: static or shared
if(USE_DYNAMIC_LIBS)
    set(CRYPTOWRAPLIB cryptowrap_shared)
else()
    set(CRYPTOWRAPLIB cryptowrap_static)
endif()

# Libraries needed for unit testing
set(UNIT_TEST_LIBS
    ${CRYPTOWRAP_LIBRARIES}    # the libraries libcryptowrap depends on
    ${LIBCHECK_LIBRARY}         # libcheck
)
# SunOS + SunPro needs the math library
if(CMAKE_C_COMPILER_ID STREQUAL "SunPro" AND ${CMAKE_SYSTEM_NAME} STREQUAL "SunOS")
    list(APPEND UNIT_TEST_LIBS "-lm")
endif()

# Location of the test data
add_definitions(-DTESTDATA_PATH=${TESTDIR})

# this is where the test executables are, needed for add_test() 
set(TESTBINDIR ${CMAKE_CURRENT_BINARY_DIR})

# Test programs
set(TESTPROGRAMS
    utiltest
    digesttest
    pkcrypttest
    passwordtest
    symcrypttest
)

foreach(TESTPROG ${TESTPROGRAMS})
    add_executable(${TESTPROG} EXCLUDE_FROM_ALL ${TESTPROG}.c)
    target_link_libraries(${TESTPROG} ${CRYPTOWRAPLIB} ${UNIT_TEST_LIBS})
        flag_fix(${TESTPROG})
        add_test(${TESTPROG} ${TESTBINDIR}/${TESTPROG})
endforeach(TESTPROG)

# add to global unit test target list
set(UNIT_TEST_TARGETS 
    ${UNIT_TEST_TARGETS}
    ${TESTPROGRAMS}
    CACHE INTERNAL "unit test targets"
)
