/* == TEST PROGRAM digesttest.c == */

/**
 * Tests the "digest" module of the CryptoWrap library.
 * \author Andras Aszodi
 * \date 2013-06-10
 */

/* -- Own headers -- */

#include "cryptowrap/digest.h"
#include "cryptowrap/utils.h"
#include "cryptowrap/config.h"

/* -- Testing headers -- */

#include "check.h"

/* -- Standard headers -- */

#include <stdlib.h>

/* == TESTS == */

/* -- Digest tests -- */

START_TEST(digest_sha512)
{
    char *hexdigest = NULL;
    unsigned int digest_len = 0;
    uint8_t *rawdigest = cryptowrap_digest(
            (const uint8_t*)"This is a message", 17,
            "sha512",
            &digest_len
    );
    ck_assert_ptr_ne(rawdigest, NULL);
    ck_assert_int_eq(digest_len, 64);
    
    hexdigest = cryptowrap_raw2hex(rawdigest, 64);
    ck_assert_str_eq(hexdigest,
        "2a5b6cf80e8a2ef047e1b68911893362e43763bb45185d66f7ac1aea3f14ba293a2985bf2ab9771228a4b0890846dbff7d86502ad1125a19d21784536bf5324d"
    );
    free(hexdigest);
    free(rawdigest);
}
END_TEST

START_TEST(nosuch_digest)
{
    unsigned int digest_len = 0;
    uint8_t *rawdigest = cryptowrap_digest(
            (const uint8_t*)"This is a message", 17,
            "no such digest",
            &digest_len
    );
    ck_assert_ptr_eq(rawdigest, NULL);
    ck_assert_int_eq(digest_len, 0);
}
END_TEST

/* -- Signing and verification -- */

static void sign_verify(
        const char* digestname,
        const EVP_PKEY* privkey,
        const EVP_PKEY* pubkey
) {
    const uint8_t MESSAGE[17] = "This is a message";
    const unsigned int MESSAGELEN = 17;
    
    uint8_t *signature = NULL;
    unsigned int signature_len = 0,
        verifyresult = 0;
    
    /* Sign the message */
    signature = cryptowrap_sign(MESSAGE, MESSAGELEN,
        digestname, privkey,
        &signature_len
    );
    ck_assert_ptr_ne(signature, NULL);
    ck_assert_int_ne(signature_len, 0); /* signature lengths vary */
    
    /* verify the message */
    verifyresult = cryptowrap_verify(MESSAGE, MESSAGELEN,
        digestname,
        signature, signature_len,
        pubkey
    );
    free(signature);
    ck_assert_int_eq(verifyresult, 1);
}

START_TEST(signverify_rsa)
{
    EVP_PKEY *privkey, *pubkey;
    
    privkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_priv.pem ), 
        1);
    ck_assert_ptr_ne(privkey, NULL);
    
    pubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_rsa_pub.pem ), 
        0);
    ck_assert_ptr_ne(pubkey, NULL);
    
    sign_verify("md5", privkey, pubkey);
    sign_verify("sha256", privkey, pubkey);
    
    EVP_PKEY_free(privkey);
    EVP_PKEY_free(pubkey);
}
END_TEST

START_TEST(signverify_dsa)
{
    EVP_PKEY *privkey, *pubkey;
    
    privkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_dsa_priv.pem ), 
        1);
    ck_assert_ptr_ne(privkey, NULL);
    
    pubkey = cryptowrap_readkey(
        TOSTRING(TESTDATA_PATH/testkey_dsa_pub.pem ), 
        0);
    ck_assert_ptr_ne(pubkey, NULL);
    
    sign_verify("dss1", privkey, pubkey);
    /* sign_verify("sha256", privkey, pubkey); */  /* this will fail in OpenSSL <= 1.0 */
    
    EVP_PKEY_free(privkey);
    EVP_PKEY_free(pubkey);
}
END_TEST

Suite *util_suite(void) {
    Suite *suite = suite_create("Digest");
    TCase *tc_digest = tcase_create("Digest"),
            *tc_signverify = tcase_create("SignVerify");
    
    /* OpenSSL init/cleanup runs only for the whole test case */
    tcase_add_unchecked_fixture (tc_digest, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_digest, digest_sha512);
    tcase_add_test(tc_digest, nosuch_digest);
    suite_add_tcase(suite, tc_digest);
    
    tcase_add_unchecked_fixture (tc_signverify, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_signverify, signverify_rsa);
    tcase_add_test(tc_signverify, signverify_dsa);
    suite_add_tcase(suite, tc_signverify);
    
    return suite;
}

/* == MAIN == */

int main(int argc, char* argv[]) {
    int numfailed;
    Suite *suite = util_suite();
    SRunner *runner = srunner_create(suite);
    
    srunner_run_all (runner, CK_NORMAL);
    numfailed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (numfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
