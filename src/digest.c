/* == MODULE digest.c == */

/** 
 * In-memory message digest calculations.
 * \author Andras Aszodi
 * \date 2013-06-10
 */

/* -- Own header -- */

#include "cryptowrap/digest.h"
#include "cryptowrap/utils.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

/* -- Standard headers -- */

#include <stdio.h>
#include <string.h>

/* -- Private prototypes -- */

/*
 * Checks if a digest and the key are compatible for the purposes of digital signing.
 * This is necessary because at least with OpenSSL 0.9.x 
 * it turns out that DSA keys need the DSS1 digest for signing,
 * RSA seems to work with any reasonable hash.
 * \param digestname The name of the digest
 * \param key The key
 * \return 1 if the digest and the key are compatible, 0 otherwise.
 */
static int digest_key_compat(
        const char* digestname,
        const EVP_PKEY* key
);

/* == Implementation == */

/* -- Public functions -- */

/*
 * Note on the usage of goto:
 * Once the context is allocated, it needs to be de-allocated before the function returns.
 * I do this cleanup only once at the end of the functions,
 * labelled clearly as CLEANUP_RETURN.
 * The price we pay is to use "goto CLEANUP_RETURN"
 * at the intermediate error checks.
 */

uint8_t* cryptowrap_digest(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        unsigned int *digest_len
) {
    EVP_MD_CTX *digest_ctx = NULL;
    const EVP_MD *digest = NULL;
    uint8_t *digest_value = NULL;
    int retval, error;
    
    *digest_len = 0;    /* should stay 0 until success */
    
    /* select the digest by name */
    digest = EVP_get_digestbyname(digestname);
    if (digest == NULL) {
        fprintf(stderr, "! cryptowrap_digest(): Digest \"%s\" not found\n", digestname);
        return NULL;    /* no context to be destroyed yet, can return immediately */
    }
    
    /* init digestion */
    digest_ctx = EVP_MD_CTX_create();
    retval = EVP_DigestInit_ex(digest_ctx, digest, NULL);
    error = cryptowrap_errorcheck("EVP_DigestInit_ex()", retval, 1);
    if (error) 
        goto CLEANUP_RETURN;
    
    /* this can be done more than once for big messages */
    retval = EVP_DigestUpdate(digest_ctx, message, message_len);
    error = cryptowrap_errorcheck("EVP_DigestUpdate()", retval, 1);
    if (error) 
        goto CLEANUP_RETURN;
    
    /* finalize */
    digest_value = (uint8_t*) calloc(EVP_MAX_MD_SIZE, sizeof(uint8_t));
    retval = EVP_DigestFinal_ex(digest_ctx, digest_value, digest_len);
    error = cryptowrap_errorcheck("EVP_DigestFinal_ex()", retval, 1);
    if (error)
    {
        free(digest_value);
        digest_value = NULL;
        *digest_len = 0;
    }
    
    CLEANUP_RETURN:
    EVP_MD_CTX_destroy(digest_ctx);
    return digest_value;
}

uint8_t* cryptowrap_sign(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        const EVP_PKEY *privkey,
        unsigned int *signature_len
) {
    EVP_MD_CTX *sign_ctx = NULL;
    const EVP_MD *digest = NULL;
    uint8_t *signature = NULL;
    int retval, error;
    
    *signature_len = 0;
    
    /* check digest/key compatibility */
    if (!digest_key_compat(digestname, privkey)) {
        fprintf(stderr, "! cryptowrap_sign(): Digest \"%s\" not compatible with key\n", digestname);
        return NULL;    /* no context yet */
    }
    
    /* select the digest by name */
    digest = EVP_get_digestbyname(digestname);
    if (digest == NULL) {
        fprintf(stderr, "! cryptowrap_sign(): Digest \"%s\" not found\n", digestname);
        return NULL;    /* no context yet */
    }
    
    /* init signing */
    sign_ctx = EVP_MD_CTX_create();
    retval = EVP_SignInit(sign_ctx, digest);
    error = cryptowrap_errorcheck("EVP_SignInit()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* this can be done more than once for big messages */
    retval = EVP_SignUpdate(sign_ctx, message, message_len);
    error = cryptowrap_errorcheck("EVP_SignUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finalize */
    signature = (uint8_t*) calloc(EVP_PKEY_size(privkey), sizeof(uint8_t));
    retval = EVP_SignFinal(sign_ctx, signature, signature_len, privkey);
    error = cryptowrap_errorcheck("EVP_SignFinal()", retval, 1);
    if (error) {
        free(signature);
        signature = NULL;
        *signature_len = 0;
    }
    
    CLEANUP_RETURN:
    EVP_MD_CTX_destroy(sign_ctx);
    return signature;
}

int cryptowrap_verify(
        const uint8_t *message,
        unsigned int message_len,
        const char *digestname,
        const uint8_t *signature,
        unsigned int signature_len,
        const EVP_PKEY *pubkey
) {
    EVP_MD_CTX *verify_ctx = NULL;
    const EVP_MD *digest = NULL;
    int retval, error, verifyresult = -1;
    
    /* check digest/key compatibility */
    if (!digest_key_compat(digestname, pubkey)) {
        fprintf(stderr, "! cryptowrap_verify(): Digest \"%s\" not compatible with key\n", digestname);
        return -1;    /* no context yet */
    }
    
    /* select the digest by name */
    digest = EVP_get_digestbyname(digestname);
    if (digest == NULL) {
        fprintf(stderr, "! cryptowrap_verify(): Digest \"%s\" not found\n", digestname);
        return -1;  /* no context yet */
    }
    
    /* init verification */
    verify_ctx = EVP_MD_CTX_create();
    retval = EVP_VerifyInit(verify_ctx, digest);
    error = cryptowrap_errorcheck("EVP_VerifyInit()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* this can be done more than once for big messages */
    retval = EVP_VerifyUpdate(verify_ctx, message, message_len);
    error = cryptowrap_errorcheck("EVP_VerifyUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finalize */
    verifyresult = EVP_VerifyFinal(verify_ctx, signature, signature_len, pubkey);
    if (verifyresult < 0) {
        cryptowrap_errorcheck("EVP_VerifyFinal()", retval, 1);
        verifyresult = -1;
    }
    
    CLEANUP_RETURN:
    EVP_MD_CTX_destroy(verify_ctx);
    return verifyresult;
}

/* -- Private functions -- */

static int digest_key_compat(
        const char* digestname,
        const EVP_PKEY* key
) {
    int retval = 0;
    int keytype = EVP_PKEY_type(((EVP_PKEY*)key)->type);
    
    switch (keytype) {
        case EVP_PKEY_RSA:
            retval = strcmp(digestname, "dss1");    /* all but DSS1 */
            break;
        case EVP_PKEY_DSA:
            retval = !strcmp(digestname, "dss1");   /* only DSS1, at least OpenSSL 0.9.x */
            break;
        case EVP_PKEY_DH:
        case EVP_PKEY_EC:
        default:
            retval = 0;
            break;
    }
    return retval;
}

