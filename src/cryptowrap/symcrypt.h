#ifndef CRYPTOWRAP_SYMCRYPT_HEADER
#define CRYPTOWRAP_SYMCRYPT_HEADER

/* == HEADER cryptowrap/symcrypt.h == */

/** 
 * \file symcrypt.h
 * \brief In-memory symmetric encryption/decryption.
 * \author Andras Aszodi
 * \date 2013-06-18
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"
#include "cryptowrap/symkey.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

#ifdef __cplusplus
extern "C" {
#endif

/* == Prototypes == */

/**
 * Generates a symmetric encryption key from a given _password_ according to PBKDF2.
 * \param password The password, should be a \0-terminated string. The longer the better :-)
 * \param ciphername The name of the cipher to be used, e.g. "aes-256-cbc". This determines
 * the key and IV lengths.
 * \param salt An array of raw bytes containing a salt.
 * \param saltlen The length of `salt`.
 * \param itercount Iteration count, use a large number to slow down brute-force attacks.
 * If `itercount == 0`, then a default count (1000) will be used.
 * \param symkey Pointer to a `cryptowrap_symkey_t` structure
 * which will be initialised appropriately so no previous invocation of
 * `cryptowrap_init_symkey()` is necessary.
 * \return 1 if all went well, 0 on error.
 */
int cryptowrap_keygen(
        const char *password,
        const char *ciphername,
        const uint8_t *salt,
        unsigned int saltlen,
        unsigned int itercount,
        cryptowrap_symkey_t *symkey
);

/**
 * Encrypts an in-memory raw byte array using symmetric encryption.
 * \param plain The plaintext raw byte array
 * \param plainlen The length of `plain`
 * \param ciphername The human-readable cipher name, e.g. "aes-256-cbc"
 * \param symkey A structure holding the key and init vector (IV) byte arrays,
 *  see "symkey.h" for details. Such key objects can be generated
 *  from password strings using `cryptowrap_pbkdf2(...)`. Note that 
 *  the key and IV arrays in `symkey` must have lengths that correspond
 *  to the requirementes of the cipher algorithm used: e.g. AES256 expects
 *  a 32-byte key and a 16-byte IV.
 * \param cryptlen pointer to the length of the returned crypt array.
 * \return An array of raw bytes allocated within, containing the encrypted bytes.
 * The length of the array is returned in `*cryptlen`. On error `NULL` is returned
 * and `*cryptlen == 0`. Do not forget to `free()` the returned array after use.
 * \sa cryptowrap_symdecrypt()
 */
uint8_t *cryptowrap_symencrypt(
        const uint8_t *plain,
        unsigned int plainlen,
        const char* ciphername,
        const cryptowrap_symkey_t *symkey,
        unsigned int *cryptlen
);

/**
 * Decrypts an in-memory raw byte array using symmetric encryption. This is the
 * inverse operation of `cryptowrap_symencrypt(...)`.
 * \param crypt The encrypted array of raw bytes.
 * \param cryptlen The length of `crypt`.
 * \param ciphername The human-readable cipher name, e.g. "aes_256_cbc".
 * This obviously has to be the same as what was used during encryption.
 * \param symkey A structure holding the key and init vector (IV) byte arrays.
 * Obviously this has to be the same as what was used during encryption,
 * and it has to be compatible to the cipher.
 * \param plainlen A pointer to the length of the returned plaintext array.
 * \return An array of raw bytes allocated within, containing the decrypted bytes.
 * The length of the array is returned in `*plainlen`. On error `NULL` is returned
 * and `*plainlen == 0`. Do not forget to `free()` the returned array after use.
 * \sa cryptowrap_symencrypt()
 */
uint8_t* cryptowrap_symdecrypt(
        const uint8_t *crypt,
        unsigned int cryptlen,
        const char* ciphername,
        const cryptowrap_symkey_t *symkey,
        unsigned int *plainlen
);

#ifdef __cplusplus
}
#endif

#endif  /* CRYPTOWRAP_SYMCRYPT_HEADER */
