/* == MODULE symkey.c == */

/** 
 * Symmetric key handling.
 * \author Andras Aszodi
 * \date 2013-06-14
 */

/* -- Own header -- */

#include "cryptowrap/symkey.h"
#include "cryptowrap/utils.h"

/* -- OpenSSL headers -- */

#include "openssl/rand.h"
#include "openssl/x509.h"
#include "openssl/hmac.h"

/* -- Standard headers -- */

#include <stdio.h>

/* == Implementation == */

unsigned int cryptowrap_init_symkey(
        cryptowrap_symkey_t *symkey,
        unsigned int keylen,
        unsigned int ivlen,
        unsigned int saltlen
) {
    unsigned int totallen;
    
    if (keylen == 0) {
        fputs("! cryptowrap_init_symkey(): keylen may not be 0\n", stderr);
        return 0;
    }
    totallen = keylen + ivlen + saltlen;
    symkey->key = (uint8_t *) calloc(totallen, sizeof(uint8_t)); /* inits to 0 */
    symkey->keylen = keylen;
    
    if (ivlen > 0) {
        symkey->iv = symkey->key + keylen;
    } else {
        symkey->iv = NULL;
    }
    symkey->ivlen = ivlen;
    
    if (saltlen > 0) {
        symkey->salt = symkey->key + keylen + ivlen;
    } else {
        symkey->salt = NULL;
    }
    symkey->saltlen = saltlen;
    
    return totallen;
}

void cryptowrap_destroy_symkey(cryptowrap_symkey_t *symkey) {
    free(symkey->key);
    symkey->key = symkey->iv = symkey->salt = NULL;
    symkey->keylen = symkey->ivlen = symkey->saltlen = 0;
}
