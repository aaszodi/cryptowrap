/* == MODULE symcrypt.c == */

/** 
 * Symmetric key-based encryption.
 * \author Andras Aszodi
 * \date 2013-06-18
 */

/* -- Own header -- */

#include "cryptowrap/symcrypt.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

/* -- Standard headers -- */

#include <stdio.h>
#include <string.h>

/* -- Private prototypes -- */

/**
 * Checks if the key and IV length requirements of a given cipher
 * match what is supplied by a symmetric key structure.
 * Prints error messages to stderr if necessary.
 * \param cipher The cipher to be tested
 * \param symkey a CryptoWrap symmetric key structure.
 * \return 0 if OK, 1 if symkey->keylen is wrong, 2 if symkey->ivlen is wrong,
 * 3 if both are wrong.
 */
static int keyivlen_cipher_mismatch(
        const EVP_CIPHER *cipher,
        const cryptowrap_symkey_t *symkey
);

/* == Implementation == */

int cryptowrap_keygen(
        const char *password,
        const char *ciphername,
        const uint8_t *salt,
        unsigned int saltlen,
        unsigned int itercount,
        cryptowrap_symkey_t *symkey
) {
    static const unsigned int DEFAULT_ITERCOUNT = 1000;
    const EVP_CIPHER *cipher = NULL;
    unsigned int keylen, ivlen;
    int retval, error;
    
    /* figure out the key and IV lengths */
    cipher = EVP_get_cipherbyname(ciphername);
    if (cipher == NULL) {
        fprintf(stderr, "! cryptowrap_pbkdf2(): Cipher \"%s\" not found.", ciphername);
        return 0;
    }
    keylen = EVP_CIPHER_key_length(cipher);
    ivlen = EVP_CIPHER_iv_length(cipher);
    
    /* set up the key structure */
    retval = cryptowrap_init_symkey(symkey, keylen, ivlen, saltlen);
    if (retval == 0) {
        fputs("! cryptowrap_pbkdf2(): Cannot init symkey struct\n", stderr);
        return 0;
    }        
    memcpy(symkey->salt, salt, saltlen);
    
    retval = PKCS5_PBKDF2_HMAC_SHA1(
        password, strlen(password), 
        symkey->salt, saltlen,
        itercount, 
        keylen + ivlen, symkey->key
    );
    error = cryptowrap_errorcheck("PKCS5_PBKDF2_HMAC_SHA1()", retval, 1);
    if (error) {
        cryptowrap_destroy_symkey(symkey);
        return 0;
    }
    
    return 1;   /* OpenSSL OK */
}

uint8_t *cryptowrap_symencrypt(
        const uint8_t *plain,
        unsigned int plainlen,
        const char* ciphername,
        const cryptowrap_symkey_t *symkey,
        unsigned int *cryptlen
) {
    EVP_CIPHER_CTX ctx;
    const EVP_CIPHER *cipher = NULL;
    unsigned int blocksize, clen = 0;
    uint8_t *crypt = NULL;  /* buffer for encrypted message */
    int retval = 1, error = 0;
    
    cipher = EVP_get_cipherbyname(ciphername);
    if (cipher == NULL) {
        fprintf(stderr, "! cryptowrap_symencrypt(): Cipher \"%s\" not found.", ciphername);
        return NULL;
    }
    
    /* check the key and IV length requirements of the cipher */
    retval = keyivlen_cipher_mismatch(cipher, symkey);
    if (retval != 0)
        return NULL;
    
    /* init */
    EVP_CIPHER_CTX_init(&ctx);
    retval = EVP_EncryptInit_ex(&ctx, cipher, 
        NULL, symkey->key, symkey->iv
    );
    error = cryptowrap_errorcheck("EVP_EncryptInit_ex()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* encrypt. This could be repeated for several rounds */
    blocksize = EVP_CIPHER_block_size(cipher);
    crypt = (uint8_t *) malloc(plainlen + blocksize);
    *cryptlen = 0;
    retval = EVP_EncryptUpdate(&ctx, 
        crypt + *cryptlen,   /* part of buffer that's still empty */
        &clen,      /* this much is written in this round */
        plain, plainlen
    );
    *cryptlen += clen; /* keep track of bytes written in this round */
    error = cryptowrap_errorcheck("EVP_EncryptUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finish. This also writes to the end of the buffer */
    retval = EVP_EncryptFinal(&ctx, 
        crypt + *cryptlen, 
        &clen
    );
    *cryptlen += clen; /* keep track of bytes written in the finalization round */
    error = cryptowrap_errorcheck("EVP_EncryptFinal()", retval, 1);
    
    CLEANUP_RETURN:
    if (error) {
        free(crypt); crypt = NULL; *cryptlen = 0;
    }
    EVP_CIPHER_CTX_cleanup(&ctx);
    return crypt;
}

uint8_t* cryptowrap_symdecrypt(
        const uint8_t *crypt,
        unsigned int cryptlen,
        const char* ciphername,
        const cryptowrap_symkey_t *symkey,
        unsigned int *plainlen
) {
    EVP_CIPHER_CTX ctx;
    const EVP_CIPHER *cipher = NULL;
    uint8_t *plain = NULL;  /* decrypted raw bytes */
    unsigned int blocksize, plen = 0;
    int retval, error = 0;
    
    *plainlen = 0;

    cipher = EVP_get_cipherbyname(ciphername);
    if (cipher == NULL) {
        fprintf(stderr, "! cryptowrap_symdecrypt(): Cipher \"%s\" not found.", ciphername);
        return NULL;
    }
    
    /* check the key and IV length requirements of the cipher */
    retval = keyivlen_cipher_mismatch(cipher, symkey);
    if (retval != 0)
        return NULL;
    
    /* init */
    EVP_CIPHER_CTX_init(&ctx);
    retval = EVP_DecryptInit_ex(&ctx, cipher, 
        NULL, symkey->key, symkey->iv
    );
    error = cryptowrap_errorcheck("EVP_DecryptInit()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* decrypt. This could be repeated for several rounds */
    blocksize = EVP_CIPHER_block_size(cipher);
    plain = (uint8_t *) malloc(cryptlen + blocksize);
    retval = EVP_DecryptUpdate(&ctx, 
        plain + *plainlen,  /* offset to output buffer empty part */
        &plen,      /* this much is written in this round */
        crypt, cryptlen
    );
    *plainlen += plen;  /* keep track of output buffer length */
    error = cryptowrap_errorcheck("EVP_DecryptUpdate()", retval, 1);
    if (error)
        goto CLEANUP_RETURN;
    
    /* finish */
    retval = EVP_DecryptFinal(&ctx, 
        plain + *plainlen,
        &plen
    );
    *plainlen += plen;  /* final round */
    error = cryptowrap_errorcheck("EVP_DecryptFinal()", retval, 1);
    
    CLEANUP_RETURN:
    if (error) {
        free(plain);
        plain = NULL;
        *plainlen = 0;
    }
    EVP_CIPHER_CTX_cleanup(&ctx);
    return plain;
}

/* == Private implementation details == */

static int keyivlen_cipher_mismatch(
        const EVP_CIPHER *cipher,
        const cryptowrap_symkey_t *symkey
) {
    unsigned int keylen = EVP_CIPHER_key_length(cipher),
            ivlen = EVP_CIPHER_iv_length(cipher);
    int retval = 0;
    if (keylen != symkey->keylen) {
        fprintf(stderr, "! Key length %d provided, cipher expects %d\n", 
            symkey->keylen, keylen);
        retval += 1;
    }
    if (ivlen != symkey->ivlen) {
        fprintf(stderr, "! IV length %d provided, cipher expects %d\n", 
            symkey->ivlen, ivlen);
        retval += 2;
    }
    return retval;
}
