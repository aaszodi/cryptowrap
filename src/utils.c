/* == MODULE utils.c == */

/*
 * Utility functions for setting up OpenSSL.
 * \author Andras Aszodi
 * \date 2013-06-09
 */

/* -- Own header -- */

#include "cryptowrap/utils.h"

/* -- OpenSSL headers -- */

#include "openssl/conf.h"
#include "openssl/bio.h"
#include "openssl/err.h"

/* -- Standard headers -- */

#include <stdio.h>
#include <string.h>

/* -- Private prototypes -- */

/* 
 * Returns the expected length of the raw data decoded from a Base64 string.
 * The input string is not supposed to be containing newline formatting.
 */
static unsigned int base64_decodelen(const char* b64str);

/* == Implementation == */

void cryptowrap_init(void) {
    OPENSSL_config(NULL);   /* load config file defined in env var OPENSSL_CONF: note all caps! */
    OpenSSL_add_all_algorithms();   /* not sure about this */
    OpenSSL_add_all_digests();  /* digest algorithm names */
    ENGINE_load_builtin_engines();  /* make all built-in engines available */
    ENGINE_register_all_complete();
    ERR_load_crypto_strings();  /* human-readable error messages */
}

int cryptowrap_errorcheck(
        const char *funcname,
        int retval,
        int successval
) {
    if (retval != successval) {
        fprintf(stderr, "! %s returned %d, expected %d\n", funcname, retval, successval);
        fputs("! OpenSSL error: ", stderr);
        ERR_print_errors_fp(stderr);
        return ERR_get_error();
    }
    return 0;   /* OK */
}

EVP_PKEY *cryptowrap_readkey(
        const char *filename,
        int keytype
) {
    FILE *keyf = NULL;
    EVP_PKEY *key = NULL;
    
    keyf = fopen(filename, "r");
    if (keyf == NULL) {
        fprintf(stderr, "! cryptowrap_readkey(\"%s\", %d): Cannot open key file\n", filename, keytype);
        return NULL;
    }
    
    if (keytype == 0) {
        key = PEM_read_PUBKEY(keyf, NULL, NULL, NULL);
    } else {
        key = PEM_read_PrivateKey(keyf, NULL, NULL, NULL);
    }
    fclose(keyf);
    
    if (key == NULL) {
        fprintf(stderr, "! cryptowrap_readkey(\"%s\", %d): Cannot read key\n", filename, keytype);
        return NULL;
    }
    
    return key;
}

char* cryptowrap_raw2hex(
        const uint8_t *raw,
        unsigned int rawlen
) {
    unsigned int i;
    char *hexstr = (char *) calloc(2*rawlen + 1, sizeof(char));
    
    for (i = 0; i < rawlen; ++i) {
        sprintf(hexstr + 2*i, "%02x", raw[i]);
    }
    hexstr[2*rawlen] = '\0';    /* for safety's sake */
    return hexstr;
}

uint8_t* cryptowrap_hex2raw(
        const char *hexstr,
        unsigned int *rawlen
) {
    unsigned int i;
    uint8_t *raw = NULL;
    
    *rawlen = strlen(hexstr) / 2;   /* even number of chars assumed, not tested */
    raw = (uint8_t *) calloc(*rawlen, sizeof(uint8_t));
    for (i = 0; i < *rawlen; ++i) {
        sscanf(hexstr + 2*i, "%02x", raw + i);
    }
    return raw;
}

char* cryptowrap_raw2base64(
        const uint8_t *raw,
        unsigned int rawlen
) {
    BIO *bmem, *b64;
    BUF_MEM *bmembuf;
    char *result;
    unsigned int bmembuflen = 0;
    
    /* set up a BIO stack: it encodes if you write to it */
    b64 = BIO_new(BIO_f_base64());  /* Base64 encoder if written to, decoder if read from */
    bmem = BIO_new(BIO_s_mem());    /* BIO sink in memory */
    BIO_push(b64, bmem);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL); /* no newline after every 64 char */
    
    /* encode */
    BIO_write(b64, raw, rawlen);
    BIO_flush(b64);     /* needed for padding */
    
    /* copy output */
    BIO_get_mem_ptr(bmem, &bmembuf);   /* memory buffer structure */
    bmembuflen = bmembuf -> length;
    result = (char *) calloc(bmembuflen + 1, sizeof(char));    /* fills with \0 */
    memcpy(result, bmembuf -> data, bmembuflen);
    BIO_free_all(b64);
    return result;
}

uint8_t *cryptowrap_base642raw(
    const char* b64str,
    unsigned int* rawlen
) {
    BIO *bmem, *b64;
    BUF_MEM *bmembuf;
    uint8_t *raw = NULL;
    
    /* allocate result */
    *rawlen = base64_decodelen(b64str);
    raw = (uint8_t *) calloc(*rawlen, sizeof(uint8_t));

    /* set up a BIO stack: it decodes if you read from it */
    b64 = BIO_new(BIO_f_base64());  /* Base64 encoder if written to, decoder if read from */
    bmem = BIO_new_mem_buf(b64str, -1); /* b64str is \0-terminated */
    BIO_push(b64, bmem); /* read from bmem, decode by b64 */
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL); /* no newline after every 64 char */
    
    /* decode */
    BIO_read(b64, raw, *rawlen);
    
    BIO_free_all(bmem);
    return raw;
}

static unsigned int base64_decodelen(const char* b64str) {
    static const char PADCH = '=';
    unsigned int padding = 0, len = strlen(b64str);
    
    /* 0, 1 or 2 '=' padding characters at the end of `b64str` */
    if (b64str[len - 1] == PADCH) {
        ++padding;
        if (b64str[len - 2] == PADCH) {
            ++padding;
        }
    }
    return (unsigned int)(len * 0.75) - padding;
}

void cryptowrap_cleanup(void) {
    ERR_remove_state(0);
    ERR_free_strings();
    
    ENGINE_cleanup();
    EVP_cleanup();
    
    CONF_modules_finish();
    CONF_modules_free();
    CONF_modules_unload(1);
    
    CRYPTO_cleanup_all_ex_data();
}
