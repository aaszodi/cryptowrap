/* == TEST PROGRAM passwordtest.c == */

/**
 * Tests the "password" module of the CryptoWrap library.
 * \author Andras Aszodi
 * \date 2013-06-15
 */

/* -- Own headers -- */

#include "cryptowrap/password.h"
#include "cryptowrap/utils.h"
#include "cryptowrap/config.h"

/* -- Testing headers -- */

#include "check.h"

/* -- Standard headers -- */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* == TESTS == */

static bool check_charclasses(const char* password, int flags) {
    static const char SPECIALS[] = "!#$%&*+/@?";
    unsigned int p = 0, lc = 0, uc = 0, dc = 0, sc = 0;
    char pch;
    bool withup = (flags & PWD_WITHUPPER),
            withdigit = (flags & PWD_WITHDIGIT),
            withspec = (flags & PWD_WITHSPEC);
        
    for (p = 0; p < strlen(password); ++p) {
        pch = password[p];
        if (islower(pch)) lc += 1;
        else if (isupper(pch)) uc += 1;
        else if (isdigit(pch)) dc += 1;
        else if (strchr(SPECIALS, pch) != NULL) sc += 1;
        else
            return false;   /* char not in any of the allowed classes */
    }
    if (lc == 0)
        return false;  /* always must have >= 1 lowercase char */
    if (withup && uc == 0 || !withup && uc > 0)
        return false;
    if (withdigit && dc == 0 || !withdigit && dc > 0)
        return false;
    if (withspec && sc == 0 || !withspec && sc > 0)
        return false;
    return true;
}

static void test_password(unsigned int pwlen, int flags) {
    char *password = NULL;
    int retval = 0;
    bool classcheck;
    
    password = (char *) calloc(pwlen + 1, sizeof(char));
    retval = cryptowrap_genpassword(password, pwlen, flags);
    ck_assert_int_eq(retval, 1);
    ck_assert_int_eq(strlen(password), pwlen);
    classcheck = check_charclasses(password, flags);
    if (!classcheck)
        fprintf(stderr, "Bad password (flags = %d): \"%s\"\n", flags, password);
    ck_assert(classcheck);
    free(password);
}

START_TEST(pwgen_classes)
{
    static const unsigned int PWLEN = 16;
    
    test_password(PWLEN, PWD_WITHUPPER);
    test_password(PWLEN, PWD_WITHDIGIT);
    test_password(PWLEN, PWD_WITHSPEC);
    test_password(PWLEN, PWD_WITHUPPER | PWD_WITHDIGIT);
    test_password(PWLEN, PWD_WITHALL);
}
END_TEST

START_TEST(verylong_password)
{
    /* the random buffer is only 64 bytes long which is usually OK
     * but let's exercise its multiple invocation */
    static const unsigned int PWLEN = 256;
    
    test_password(PWLEN, PWD_WITHALL);
}
END_TEST

START_TEST(passwordhash)
{
    /* RFC 6070, example 3:
        Input:
        P = "password" (8 octets)
        S = "salt" (4 octets)
        c = 4096
        dkLen = 20

        Output:
        DK = 4b 00 79 01 b7 65 48 9a
            be ad 49 d9 26 f7 21 d0
            65 a4 29 c1             (20 octets)
    */
    #define HASHLEN 20
    const uint8_t HASH[HASHLEN] = {
        0x4b, 0x00, 0x79, 0x01, 0xb7, 0x65, 0x48, 0x9a,
        0xbe, 0xad, 0x49, 0xd9, 0x26, 0xf7, 0x21, 0xd0,
        0x65, 0xa4, 0x29, 0xc1
    };
    uint8_t hash[HASHLEN];
    int retval;
    unsigned int i;
    bool ok;
    
    retval = cryptowrap_passwordhash(
        "password", 
        "salt", 4, /* use only 4 bytes of the string */
        4096,
        20, hash
    );
    ck_assert_int_eq(retval, 1);

    ok = true;
    for (i = 0; i < HASHLEN; ++i) {
        ok = ok && (hash[i] == HASH[i]);
        if (!ok) {
            fprintf(stderr, "Hash[%d] mismatch: %x vs %x\n", 
                i, hash[i], HASH[i]);
            break;
        }
    }
    ck_assert(ok);
    #undef HASHLEN
}
END_TEST

Suite *create_suite(void) {
    Suite *suite = suite_create("Password");
    TCase *tc_password = tcase_create("Password");
    
    /* setup/teardown run only once per test case */
    tcase_add_unchecked_fixture (tc_password, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_password, pwgen_classes);
    tcase_add_test(tc_password, verylong_password);
    tcase_add_test(tc_password, passwordhash);
    suite_add_tcase(suite, tc_password);
        
    return suite;
}

/* == MAIN == */

int main(int argc, char* argv[]) {
    int numfailed;
    Suite *suite = create_suite();
    SRunner *runner = srunner_create(suite);
    
    srunner_run_all (runner, CK_NORMAL);
    numfailed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (numfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
