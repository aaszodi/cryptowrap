/* == TEST PROGRAM symcrypttest.c == */

/**
 * Tests the "pkcrypt" module of the CryptoWrap library.
 * \author Andras Aszodi
 * \date 2013-06-18
 */

/* -- Own headers -- */

#include "cryptowrap/symkey.h"
#include "cryptowrap/symcrypt.h"
#include "cryptowrap/utils.h"
#include "cryptowrap/config.h"

/* -- Testing headers -- */

#include "check.h"

/* -- Standard headers -- */

#include <stdlib.h>

/* == TESTS == */

static void test_init_symkey(
        cryptowrap_symkey_t *symkey,
        unsigned int keylen,
        unsigned int ivlen,
        unsigned int saltlen
) {
    int retval;
    retval = cryptowrap_init_symkey(symkey, keylen, ivlen, saltlen);
    ck_assert_int_eq(retval, keylen + ivlen + saltlen);
    ck_assert_int_eq(symkey->keylen, keylen);
    ck_assert_int_eq(symkey->ivlen, ivlen);
    ck_assert_int_eq(symkey->saltlen, saltlen);
    ck_assert_ptr_ne(symkey->key, NULL);
    ck_assert_ptr_eq(symkey->iv, 
        (ivlen > 0)? symkey->key + keylen: NULL);
    ck_assert_ptr_eq(symkey->salt, 
        (saltlen > 0)? symkey->key + keylen + ivlen: NULL);    
}

START_TEST(symkey)
{
    static const unsigned int keylen = 10, ivlen = 6, saltlen = 4;
    
    cryptowrap_symkey_t all, noiv, nosalt, noivnosalt;
    
    /* all 3 components are there */
    test_init_symkey(&all, keylen, ivlen, saltlen);
    
    /* test destroy operation once */
    cryptowrap_destroy_symkey(&all);
    ck_assert_int_eq(all.keylen, 0);
    ck_assert_int_eq(all.ivlen, 0);
    ck_assert_int_eq(all.saltlen, 0);
    ck_assert_ptr_eq(all.key, NULL);
    ck_assert_ptr_eq(all.iv, NULL);
    ck_assert_ptr_eq(all.salt, NULL);
    
    /* no IV */
    test_init_symkey(&noiv, keylen, 0, saltlen);
    cryptowrap_destroy_symkey(&noiv);  /* no test needed */
    
    /* no salt */
    test_init_symkey(&nosalt, keylen, ivlen, 0);
    cryptowrap_destroy_symkey(&nosalt);  /* no test needed */
    
    /* no IV, no salt */
    test_init_symkey(&noivnosalt, keylen, 0, 0);
    cryptowrap_destroy_symkey(&noivnosalt);  /* no test needed */
}
END_TEST

START_TEST(symcrypt)
{
    const char PASSWORD[] = "password", 
        CIPHER[] = "aes-256-cbc";
    const uint8_t SALT[5] = "salt\0",   /* can be any raw byte */
        MESSAGE[18] = "This is a message\0";  /* terminating \0 won't be encrypted */
    
    const unsigned int KEYLEN = 32, IVLEN = 16, /* we _know_ AES256 needs these */
        SALTLEN = 4,
        MESSAGELEN = 17;

    cryptowrap_symkey_t symkey;
    uint8_t *crypt = NULL, *plain = NULL;
    unsigned int cryptlen = 0, plainlen = 0;
    int retval;
    
    /* generate the key from the password */
    retval = cryptowrap_keygen(
        PASSWORD, CIPHER,
        SALT, SALTLEN,
        0,  /* default iteration count */
        &symkey
    );
    ck_assert_int_eq(retval, 1);

    /* encryption */
    crypt = cryptowrap_symencrypt(
        MESSAGE, MESSAGELEN,
        CIPHER, &symkey,
        &cryptlen
    );
    ck_assert_int_eq(retval, 1);
    ck_assert_ptr_ne(crypt, NULL);
    ck_assert_int_ne(cryptlen, 0);
    
    /* decrypt and compare plaintext */
    
    plain = cryptowrap_symdecrypt(
        crypt, cryptlen,
        CIPHER, &symkey,
        &plainlen
    );
    ck_assert_ptr_ne(plain, NULL);
    ck_assert_int_eq(plainlen, MESSAGELEN);
    
    /* in order to do string comparison for testing,
     * we artificially terminate the _plain_ buffer
     */
    plain[plainlen] = '\0';
    ck_assert_str_eq((const char*) plain, (const char*) MESSAGE);
    
    free(crypt); free(plain); 
}
END_TEST

Suite *util_suite(void) {
    Suite *suite = suite_create("SymCrypt");
    TCase *tc_symcrypt = tcase_create("SymCrypt");
    
    /* OpenSSL init/cleanup runs only for the whole test case */
    tcase_add_unchecked_fixture (tc_symcrypt, cryptowrap_init, cryptowrap_cleanup);
    tcase_add_test(tc_symcrypt, symkey);
    tcase_add_test(tc_symcrypt, symcrypt);
    suite_add_tcase(suite, tc_symcrypt);
    
    return suite;
}

/* == MAIN == */

int main(int argc, char* argv[]) {
    int numfailed;
    Suite *suite = util_suite();
    SRunner *runner = srunner_create(suite);
    
    srunner_run_all (runner, CK_NORMAL);
    numfailed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (numfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
