/* == PROGRAM pwgen.c == */

/**
 * \file pwgen.c 
 * \brief Random password generator.
 * \date 2013-06-16
 * \author Andras Aszodi
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"
#include "cryptowrap/password.h"

/* -- Standard headers -- */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>

/* getopt is not too easy */
#ifdef _WIN32
    #include "wgetopt.h"    /* TODO */
#else
    #if defined(__sun) && defined(__SUNPRO_CC)
        #define __EXTENSIONS__
        #include <unistd.h>     // Solaris/SunPro getopt() is here...
    #else
        #include <getopt.h> // Linux, MacOS
    #endif
#endif

/* == DEFAULT VALUES == */

#define PASSWORDLEN 16
#define PASSWORDCOUNT 1

/* == FUNCTIONS == */

static unsigned int parse_uintparam(
        const char* optarg,
        unsigned int defval
) {
    unsigned int retval;
    unsigned long num = strtoul(optarg, NULL, 10);
    if (num == 0 || errno != 0)
        return defval;
    retval = (num > UINT_MAX)? UINT_MAX: (unsigned int)num;
    return retval;
}

static void print_help() {
    fputs("Simple password generator\nUsage: pwgen -udsa [-L pwdlen] [-n pwdcount]\n", stderr);
    fputs("Character set selection options:\n", stderr);
    fputs("\t-u: Require at least one uppercase character\n", stderr);
    fputs("\t-d: Require at least one digit character\n", stderr);
    fputs("\t-s: Require at least one special character\n", stderr);
    fputs("\t-a: Equivalent to -uds\n", stderr);
    fputs("Password length and multiple password options:\n", stderr);
    fprintf(stderr, "\t-L pwdlen: password length, default %d characters\n", PASSWORDLEN);
    fprintf(stderr, "\t-n pwdcount: generate this many passwords, default %d\n", PASSWORDCOUNT);
    fputs("\t-v: Print version information and exit\n", stderr);
    fputs("\t-h: Print this help and exit\n", stderr);
}

/* == MAIN == */

int main(int argc, char *argv[]) {
    extern char* optarg;
    signed char optch;
    static const char OPTCHARS[] = "audsL:n:vh";
    pwd_flags_t flags = PWD_LOWERONLY;
    unsigned int i, retval,
        passwordlen = PASSWORDLEN,
        passwordcount = PASSWORDCOUNT;
    char *password = NULL;
    
    /* process commandline options */
    while((optch = getopt(argc, argv, OPTCHARS)) != -1)
    {
        switch(optch)
        {
            case 'a':   /* all character classes: -a == -udp */
                flags = PWD_WITHALL;
                break;
            case 'u':
                flags = flags | PWD_WITHUPPER;
                break;
            case 'd':
                flags = flags | PWD_WITHDIGIT;
                break;
            case 's':
                flags = flags | PWD_WITHSPEC;
                break;
            case 'L':   /* password length */
                passwordlen = parse_uintparam(optarg, PASSWORDLEN);
                break;
            case 'n':   /* how many passwords to generate */
                passwordcount = parse_uintparam(optarg, PASSWORDCOUNT);
                break;
            case 'v':
                print_version(stderr, "pwgen: simple password generator", __DATE__, __TIME__);
                exit(EXIT_SUCCESS);
            case 'h':
            case '?':
                print_help();
                exit(EXIT_FAILURE);
        }
    }
    if (flags == PWD_LOWERONLY) {
        fputs("! Option error: please select one of the -a,-u,-d,-s options.\n", stderr);
        print_help();
        exit(EXIT_FAILURE);
    }
    
    password = (char *) calloc(passwordlen + 1, sizeof(char));
    for (i = 0; i < passwordcount; ++i) {
        retval = cryptowrap_genpassword(password, passwordlen, flags);
        if (retval != 1)
            exit(EXIT_FAILURE);
        puts(password);
    }
    free(password);
    exit(EXIT_SUCCESS);
}
