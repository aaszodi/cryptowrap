/* == MODULE password.c == */

/** 
 * Password generation.
 * \author Andras Aszodi
 * \date 2013-06-14
 */

/* -- Own header -- */

#include "cryptowrap/password.h"
#include "cryptowrap/utils.h"

/* -- OpenSSL headers -- */

#include "openssl/rand.h"
#include "openssl/x509.h"
#include "openssl/hmac.h"

/* -- Standard headers -- */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* == Implementation == */

int cryptowrap_genpassword(
        char *password,
        unsigned int passwordlen,
        int flags
) {
    static const unsigned int DEFAULT_PASSWORDLEN = 16; 
    #define RANDBUFLEN 64
    static uint8_t RANDBUF[RANDBUFLEN];
    static const char SPECIALS[] = "!#$%&*+/@?";
    
    uint8_t ranch;      /* random character */
    unsigned int r, p,  /* indexing the random buffer and the password */
        lc, uc, dc, sc,     /* lowercase, uppercase, digit, special counts */
        pi;  /* p increment, 0 or 1 */
    bool withupper, withdigit, withspec, charok;
    int retval, error;
    
    if (passwordlen == 0)
        passwordlen = DEFAULT_PASSWORDLEN;
    withupper = flags & PWD_WITHUPPER;
    withdigit = flags & PWD_WITHDIGIT;
    withspec = flags & PWD_WITHSPEC;
    
    /* Try to make new password strings until all required char types are there */
    charok = false;
    while(!charok) {
        memset(password, '\0', passwordlen + 1);
        p = lc = uc = dc = sc = 0;
        
        /* The password chars come from OpenSSL-generated random bytes.
         * Keep making them until the password string is filled.
         */
        while(p < passwordlen) {
            retval = RAND_bytes(RANDBUF, RANDBUFLEN);
            error = cryptowrap_errorcheck("RAND_bytes()", retval, 1);
            if (error) {
                free(password);
                return 0;   /* OpenSSL error convention */
            }
            
            /* Test the random bytes until RANDBUF is exhausted or `password` is full */
            for (r = 0; r < RANDBUFLEN && p < passwordlen; ++r) {
                ranch = RANDBUF[r];
                pi = 0;
                
                /* See if `ranch` satisfies the criteria.
                 * Keep count of occurrences of the 
                 * mutually exclusive categories "uppercase", "digit", "spec"
                 * depending on what was required in `flags`
                 */
                if (islower(ranch)) {
                    pi = 1; lc += 1;
                } else if (withupper && isupper(ranch)) {
                    pi = 1; uc += 1;
                } else if (withdigit && isdigit(ranch)) {
                    pi = 1; dc += 1;
                } else if (withspec && ranch != '\0' &&
                        (strchr(SPECIALS, ranch) != NULL)   /* strchr finds '\0', too */
                ) {
                    pi = 1; sc += 1;
                }
                     
                if (pi) {
                    /* good random char, save it */
                    password[p] = ranch;
                }
                p += pi;
            }       /* end for r */
        }       /* end while p < passwordlen */
        
        /* `password` is filled: do we have all required char types? */
        charok = (lc >= 1);     /* at least 1 lowercase char is always needed */
        if (withupper) charok = charok && (uc >= 1);    /* have at least 1 uppercase char if required */
        if (withdigit) charok = charok && (dc >= 1);    /* ...same for digits */
        if (withspec) charok = charok && (sc >= 1);    /* ...and special chars */
    }       /* end while(!charok) */
    
    return 1;   /* OpenSSL OK retval */
    #undef RANDBUFLEN
}

int cryptowrap_passwordhash(
        const char* password,
        const uint8_t *salt,
        unsigned int saltlen,
        unsigned int itercount,
        unsigned int hashlen,
        uint8_t *hash
) {
    const unsigned int DEFAULT_ITERCOUNT = 1000;
    int retval, error;
    
    if (itercount == 0)
        itercount = DEFAULT_ITERCOUNT;
    
    retval = PKCS5_PBKDF2_HMAC_SHA1(
        password, strlen(password), 
        salt, saltlen, 
        itercount, 
        hashlen, hash
    );
    error = cryptowrap_errorcheck("PKCS5_PBKDF2_HMAC_SHA1()", retval, 1);
    if (error) {
        return 0;
    }
    
    return 1;   /* OpenSSL OK */
}
