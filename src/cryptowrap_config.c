/* == PROGRAM cryptowrap_config.c == */

/**
 * \file cryptowrap_config.c
 * \brief Prints CryptoWrap configuration details.
 * \date 2013-06-11
 * \author Andras Aszodi
 */

/* -- Own header -- */

#include "cryptowrap/config.h"

/* -- Standard headers -- */

#include <stdlib.h>
#include <stdio.h>

/* == MAIN == */

int main(int argc, char *argv[]) {
    print_version(stdout,
        "CryptoWrap library (c) Andras Aszodi 2013.",
        __DATE__, __TIME__
    );
    
    exit(EXIT_SUCCESS);
}
