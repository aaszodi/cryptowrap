#ifndef CRYPTOWRAP_PKCRYPT_HEADER
#define CRYPTOWRAP_PKCRYPT_HEADER

/* == HEADER cryptowrap/pkcrypt.h == */

/** 
 * \file pkcrypt.h
 * \brief In-memory encryption/decryption using public and private keys.
 * \author Andras Aszodi
 * \date 2013-06-12
 */

/* -- Own headers -- */

#include "cryptowrap/config.h"

/* -- OpenSSL headers -- */

#include "openssl/evp.h"

#ifdef __cplusplus
extern "C" {
#endif

/* == Structs == */

/**
 * The cryptowrap_envelope_t struct encapsulates the encrypted data
 * and the symmetric key encrypted with the recipient's public key.
 */
typedef struct {
    uint8_t *crypt;     /**< the encrypted data byte array */
    unsigned int cryptlen;  /**< length of _crypt_ */
    char *ciphername;   /**< the name of the symmetric cipher used */
    uint8_t *eckey;     /**< the symmetric key encrypted with the public key */
    unsigned int eckeylen;  /**< length of _eckey_ */
    uint8_t iv[EVP_MAX_IV_LENGTH];  /**< init vector */
} cryptowrap_envelope_t;

/* == Prototypes == */

/**
 * Encrypts a raw binary array using the recipient's public key.
 * \param plain The data to be encrypted, can be binary
 * \param plainlen The length of _plain_ in bytes
 * \param ciphername The name of the cipher to be used, e.g. "aes_256_cbc"
 * \param pubkey The recipient's public key, must be RSA.
 * \param envp Points to an encryption envelope structure which will be filled
 * appropriately so that it can be sent to `cryptowrap_pkdecrypt()`.
 * \return 1 on success, 0 on error (OpenSSL convention).
 */
int cryptowrap_pkencrypt(
        const uint8_t *plain,
        unsigned int plainlen,
        const char* ciphername,
        const EVP_PKEY *pubkey,
        cryptowrap_envelope_t *envp
);

/**
 * Decrypts a raw binary encrypted data array using the recipient's private key.
 * \param envp Points to an encryption envelope structure filled by `cryptowrap_pkencrypt()`.
 * It remembers all necessary settings.
 * \param privkey The recipient's private key, must be RSA.
 * \param plainlen Points to the length of the returned decrypted data array
 * \return A raw binary array allocated inside with the decrypted data,
 * or `NULL` on error, in which case `*plainlen == 0`.
 * Don't forget to deallocate the result after use.
 */
uint8_t* cryptowrap_pkdecrypt(
        const cryptowrap_envelope_t *envp,
        const EVP_PKEY *privkey,
        unsigned int *plainlen
);

/**
 * Cleans up an encryption envelope structure after use.
 * You can re-use the cleaned envelope again with `cryptowrap_pkencrypt()`.
 \param envp Pointer to the envelope to be cleaned.
 */
void cryptowrap_clear_envelope(cryptowrap_envelope_t *envp);

#ifdef __cplusplus
}
#endif

#endif  /* CRYPTOWRAP_PKCRYPT_HEADER */
